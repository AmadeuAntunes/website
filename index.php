<!DOCTYPE html>
<html lang="en">
<?php
  include './php/variaveis.php';
?>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Este site é uma um modelo sendo que apenas foi criado para fins académicos<br>
                                      O design foi criado por mim e este site está protegido por direitos autorais
                                      ">
    <meta name="author" content="Amadeu Antunes">
    <link rel="shortcut icon" href="img/logos/favicon.ico" />
    <title><?php echo $titulo; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
  
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="css/agency.min.css" rel="stylesheet">
    <link href="css/mycss.css" rel="stylesheet">
  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar  navbar-expand-lg   navbar-dark fixed-top" id="mainNav">
    <div class="container ">
     
        <a class="navbar-brand js-scroll-trigger " href="#page-top">  
            <img src="img/logos/logo.png" class="navbar-brand img-responsive logo"  alt="Logotipo"> 
        </a>
       <a class="navbar-brand  js-scroll-trigger" href="#page-top"><?php echo $ideia; ?></a>
        <button class="  navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services"><?php echo $servico; ?></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio"><?php echo $produtos;?></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about"><?php echo  $sobre; ?></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact"><?php echo $contactos ?></a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="/loja"><?php echo $loja?></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    </nav>

    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in"><?php echo $titulo; ?></div>
          <div class="intro-heading text-uppercase"><?php echo $descricao ;  ?></div>
          <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services"><?php echo $saberMais; ?></a>
        </div>
      </div>
    </header>

    <!-- Services -->
    <section id="services"  >
    <i class ="espaco"></i>
      <div class="container" >
        <div class="row">
          <div class=" col-lg-12 text-center">
            <h2 class="section-heading text-uppercase"><?php echo $servico; ?></h2>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading"><?php echo $comprar;?></h4>
            <p class="text-muted"><?php echo $comprarDesc;?></p>
          </div>
          <div class="col-md-4"> 
            <span class="fa-stack fa-4x">
              <i class="fa fa-circle fa-stack-2x text-primary"></i>
              <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading"><?php echo $qualquer;?></h4>
            <p class="text-muted"><?php echo $qualquerDesc ; ?></p>
          </div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x">
            <i class="fa fa-circle fa-stack-2x text-primary"></i>
            <i class="fa fa-paper-plane fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading"><?php echo $rapido; ?></h4>
            <p class="text-muted"><?php echo $rapidoDesc; ?></p>
          </div>
        </div>
      </div>
    </section>

    <!-- Portfolio Grid -->
    <section class="bg-light" id="portfolio">
    <i class ="espaco"></i>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase"><?php echo $produtos; ?></h2>
            <h3 class="section-subheading text-muted"><?php echo$produtosDesc; ?></h3>
          </div>
        </div>
        <div class="row">

<?php
  $i = 0;
  foreach ($arrayProdutos as &$value) {
?> 
       <div class="col-md-4 col-sm-6 portfolio-item">
            <a class="portfolio-link" data-toggle="modal" href="#portfolioModal<?php echo $i +1; ?>">
              <div class="portfolio-hover">
                <div class="portfolio-hover-content">
                  <i class="fa fa-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="img/portfolio/0<?php echo $i +1; ?>-thumbnail.jpg" alt="<?php  echo $arrayProdutos[$i]->getNomeProduto(); ?>s">
            </a>
            <div class="portfolio-caption">
              <h4><?php  echo $arrayProdutos[$i]->getNomeProduto(); ?></h4>
              <p class="text-muted"><?php  echo $arrayProdutos[$i]->getCodProduto(); ?></p>
            </div>
          </div>
<?php
      $i++;
    }
?>
        </div>
      </div>
    </section>

    <!-- About -->
    <section id="about">
    <i class ="espaco"></i>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase"><?php echo  $sobre; ?></h2>
            <p class="text-secondary large"><?php echo $sobreDesc; ?></p>
            <p class="large "><?php echo $sobreFullDesc; ?></p>
            <a href="//www.dmca.com/Protection/Status.aspx?ID=0cca7487-3ab2-4814-a2c6-3f5c879de599&refurl=http%3a%2f%2flocalhost%2fwebsite%2f&PAGE_ID=&lang=pt-BR&st=true" target="_blank" title="DMCA.com Protection Status" class="dmca-badge"> <img src="//images.dmca.com/Badges/dmca_protected_sml_120l.png?ID=0cca7487-3ab2-4814-a2c6-3f5c879de599" alt="DMCA.com Protection Status"></a> <script src="//images.dmca.com/Badges/DMCABadgeHelper.min.js"> </script>
          </div>
        </div>
      </div>
    </section>

    <!-- Contact -->
    <section id="contact">
    <i class ="espaco"></i>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase"><?php  echo $contactos;?></h2>
            <h3 class="section-subheading text-primary"><?php echo $contactosInfo; ?></h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <form id="contactForm" name="sentMessage" novalidate>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" id="name" type="text" placeholder="<?php echo $nome ?> *" required data-validation-required-message="<?php echo $nome ?>">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="email" type="email" placeholder="<?php echo $email ?> *" required data-validation-required-message="<?php echo $email ?>">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="phone" type="tel" placeholder="<?php echo $telefone ?> *" required data-validation-required-message="<?php echo $telefone ?> ">
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <textarea class="form-control" id="message" placeholder="<?php echo $mensagem ?> *" required data-validation-required-message="<?php echo $mensagem ?>"></textarea>
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <input id="sendMessageButton" class="btn btn-primary btn-xl text-uppercase" type="submit"><?php echo $enviarMensagem ?></input>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>

    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; <?php echo"http://$_SERVER[HTTP_HOST] " . date("Y"); ?></span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="https://pt.stackoverflow.com/users/20041/amadeu-antunes" target="_blank">
                  <i class="fa fa-stack-overflow"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="https://www.linkedin.com/in/amadeuantunes/" target="_blank">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="http://amadeuantunes.com/" target="_blank">
                  <i class="fa  fa-home"></i>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>

    <!-- Portfolio Modals -->

    <!-- Modal -->
    <?php
  $i = 0;
  foreach ($arrayProdutos as &$value) {
  ?> 
    <div class="portfolio-modal modal fade" id="portfolioModal<?php echo $i + 1; ?>" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase"><?php echo $arrayProdutos[$i]->getNomeProduto(); ?></h2>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/0<?php echo $i +1; ?>-full.jpg" alt="">
                  <p><?php echo $arrayProdutos[$i]->getDescProduto(); ?></p>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    <?php echo $fechar; ?></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php 
  $i++;
  }
  ?>

    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom scripts for this template -->
    <script src="js/agency.min.js"></script>
  </body>
</html>
