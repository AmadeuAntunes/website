<?php

$titulo = "T-shirt Mania";
$descricao = "À distância de um clique!";
$ideia ="Uma T-shirt, um visual!";
$servico ="Serviço";
$produtos = "Produtos";
$saberMais = "Saber Mais";
$loja = "Loja";
$comprar = "Comprar";
$qualquer = "Qualquer dispositivo";
$rapido = "Rápido e Seguro";
$comprarDesc ="Compra online e em qualquer lugar";
$qualquerDesc ="A sua loja em qualquer dispositivo com acesso à internet";
$rapidoDesc ="Envio em 48 horas de forma segura";
$produtosDesc = "Aqui apresentamos alguns exemplos dos nossos produtos";
$sobre ="Sobre Nós";
$sobreDesc ="Este é um site de demostração";
$sobreFullDesc ='
Este site foi criado para fins acadêmicos e de portfólio<br>
Todas as imagens utilizadas neste site estão sobre licença de direitos uso exclusivo para este site.<br>
Considera-se crime a copia completa ou integral de qualquer elemento gráfico e a respetiva utilização.<br>
Este site utiliza bootstrap sendo que a respetiva licença é referida  <a class="text-info" href="http://getbootstrap.com.br/v4/about/license/" target="_blank">aqui</a>.
';
$contactos = "Contacto";
$contactosInfo ="Preecha os campos abaixo";
$nome = "Introduza o seu nome";
$email = "Introduza o seu e-mail";
$telefone = "Introduza o seu numero de telefone";
$mensagem ="Escreva a sua mensagem aqui";
$enviarMensagem = "Enviar Mensagem";
$fechar = "Fechar";
$messageEnviada ="Mensagem enviada com sucesso!";


class produto{
   public $nomeProduto;
   public $codProduto;
   public $descProduto;
   public function setNomeProduto($newval)
   {
       $this->nomeProduto = $newval;
   }
   public function setCodProduto($newval)
   {
       $this->codProduto = $newval;
   }
   public function getCodProduto()
   {
     return $this->codProduto;
   }
   public function getNomeProduto()
   {
     return $this->nomeProduto;
   }
   public function setDescProduto($newval)
   {
      $this->descProduto = $newval;
   }
   public function getDescProduto()
   {
       return $this->descProduto;
   }
}
$arrayProdutos=array();
array_push($arrayProdutos, new produto());
array_push($arrayProdutos, new produto());
array_push($arrayProdutos, new produto());
array_push($arrayProdutos, new produto());
array_push($arrayProdutos, new produto());
array_push($arrayProdutos, new produto());

$arrayProdutos[0]->setNomeProduto("T-shirt Vermelha");



$arrayProdutos[1]->setNomeProduto("T-shirt Amarela");
$arrayProdutos[2]->setNomeProduto("T-shirt Azul Petróleo");
$arrayProdutos[3]->setNomeProduto("T-shirt Verde");
$arrayProdutos[4]->setNomeProduto("T-shirt Laranja");
$arrayProdutos[5]->setNomeProduto("T-shirt Azul Ciano");


$i = 0;
foreach($arrayProdutos as $value)
{
    $num_padded = sprintf("%03d", $i +1);
    $arrayProdutos[$i]->setCodProduto("PD"  . $num_padded);
    $arrayProdutos[$i]->setDescProduto("Este ".  $arrayProdutos[$i]->getNomeProduto() . " é o exemplo de uma totalmente feita em photoshop");
    $i++;
}

?>